var assert = require('assert');
const {
  By,
  until
} = require('selenium-webdriver');

module.exports = function () {
  this.Given(/^Navigating to QA$/, function () {
    return this.driver.get('https://qa.bigengine.io/');
  });

  this.Given(/^Navigating to Devel$/, function () {
    return this.driver.get('https://devel.bigengine.io/');
  });

  this.Then(/^Should log in$/, function () {
    this.driver.getTitle().then(function (title) {
      assert.equal(title, "bigfinite | Login");
      return title;
    });
    this.driver.findElement(By.name('credentials')).sendKeys('CarlosVicente2');
    this.driver.findElement(By.name('customer')).sendKeys('qa');
    this.driver.findElement(By.name('password')).sendKeys('Carlitros1242,');
    this.driver.findElement(By.css('#loginForm > button')).click();
    this.driver.findElements(By.id('navbar-masterdata'));

  });

  this.Then(/^Should not log in$/, function () {
    this.driver.getTitle().then(function (title) {
      assert.equal(title, "bigfinite | Login");
      return title;
    });
    this.driver.findElement(By.name('credentials')).sendKeys('InvalidUser');
    this.driver.findElement(By.name('customer')).sendKeys('qa');
    this.driver.findElement(By.name('password')).sendKeys('Carlitros1242,');
    this.driver.findElement(By.css('#loginForm > button')).click();
    this.driver.findElement(By.name('credentials'));
  });

  this.Then(/^Should log out$/, function () {
    return this.driver.get('https://qa.bigengine.io/logout');
  });
}