Feature: Login Bigengine
  Testing login feature.

  Scenario: Standard Login - Valid User
    Given Navigating to QA
    Then Should log in
    Then Should log out

    Scenario: Standard Login - Invalid User
    Given Navigating to QA
    Then Should not log in
    Then Should log out